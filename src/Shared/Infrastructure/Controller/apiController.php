<?php

namespace App\Shared\Infrastructure\Controller;

use App\Users\Domain\Entity\Store;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Request;
use App\Users\Domain\Entity\User;

#[Route('/api', name: 'api_')]
class apiController extends AbstractController
{
    #[Route('/users', name: 'user_index', methods:['get'] )]
    public function index(ManagerRegistry $doctrine): JsonResponse
    {
        $products = $doctrine
            ->getRepository(User::class)
            ->findAll();

        $data = [];

        foreach ($products as $product) {
            $data[] = [
                'ulid' => $product->getUlid(),
                'first_name' => $product->getFirstName(),
                'surname' => $product->getSurname(),
            ];
        }

        return $this->json($data);
    }

//
//    #[Route('/users', name: 'user_create', methods:['post'] )]
//    public function create(ManagerRegistry $doctrine, Request $request): JsonResponse
//    {
//        $entityManager = $doctrine->getManager();
//
//        $user = new User($request->request->get('first_name'),$request->request->get('surname'));
//
//
//        $entityManager->persist($user);
//        $entityManager->flush();
//
//        $data =  [
//            'ulid' => $user->getUlid(),
//            'name' => $user->getFirstName(),
//            'description' => $user->getSurname(),
//        ];
//
//        return $this->json($data);
//    }
//
//
//    #[Route('/users/{id}', name: 'user_show', methods:['get'] )]
//    public function show(ManagerRegistry $doctrine, int $id): JsonResponse
//    {
//        $user = $doctrine->getRepository(Store::class)->find($id);
//
//        if (!$user) {
//
//            return $this->json('No project found for id ' . $id, 404);
//        }
//
//        $data =  [
//            'ulid' => $user->getId(),
//            'name' => $user->getName(),
//            'description' => $user->getDescription(),
//        ];
//
//        return $this->json($data);
//    }
//
//    #[Route('/users/{id}', name: 'user_update', methods:['put', 'patch'] )]
//    public function update(ManagerRegistry $doctrine, Request $request, int $id): JsonResponse
//    {
//        $entityManager = $doctrine->getManager();
//        $user = $entityManager->getRepository(Store::class)->find($id);
//
//        if (!$user) {
//            return $this->json('No project found for id' . $id, 404);
//        }
//
//        $user->setFirstName($request->request->get('firstName'));
//        $user->setSurname($request->request->get('surname'));
//        $entityManager->flush();
//
//        $data =  [
//            'ulid' => $user->getId(),
//            'firstName' => $user->getName(),
//            'surname' => $user->getDescription(),
//        ];
//
//        return $this->json($data);
//    }
//
//    #[Route('/users/{id}', name: 'project_delete', methods:['delete'] )]
//    public function delete(ManagerRegistry $doctrine, int $id): JsonResponse
//    {
//        $entityManager = $doctrine->getManager();
//        $user = $entityManager->getRepository(Store::class)->find($id);
//
//        if (!$user) {
//            return $this->json('No project found for id' . $id, 404);
//        }
//
//        $entityManager->remove($user);
//        $entityManager->flush();
//
//        return $this->json('Deleted a project successfully with id ' . $id);
//    }

}