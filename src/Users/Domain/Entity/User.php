<?php

declare(strict_types=1);

namespace App\Users\Domain\Entity;

use App\Shared\Damain\Service\UlidService;

class User
{
    private string $ulid;
    private string $email;
    private string $password;

    public function getFirstName(): string
    {
        return $this->first_name;
    }

    public function getSurname(): string
    {
        return $this->surname;
    }
    private string $first_name;
    private string $surname;
    public function __construct(string $email, string $password,$first_name, $surname)
    {
        $this->ulid = UlidService::generate();
        $this->email = $email;
        $this->password = $password;
        $this->first_name = $first_name;
        $this->surname = $surname;
    }

    public function getUlid(): string
    {
        return $this->ulid;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function getPassword(): string
    {
        return $this->password;
    }
}