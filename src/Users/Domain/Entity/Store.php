<?php

declare(strict_types=1);

namespace App\Users\Domain\Entity;



use App\Shared\Damain\Service\UlidService;

class Store
{
    private string $ulid;
    private string $titel;
    private string $ulid_user;

    public function __construct(string $titel, string $ulid_user)
    {
        $this->ulid = UlidService::generate();
        $this->titel = $titel;
        $this->ulid_user = $ulid_user;
    }

    public function getUlid(): string
    {
        return $this->ulid;
    }

    public function getTitel(): string
    {
        return $this->titel;
    }

    public function getUlidUser(): string
    {
        return $this->ulid_user;
    }

}