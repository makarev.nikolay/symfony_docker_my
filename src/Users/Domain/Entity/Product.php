<?php

namespace App\Users\Domain\Entity;

use App\Shared\Damain\Service\UlidService;

class Product
{
    private string $ulid;
    private string $titel;
    private string $ulid_store;


    public function __construct(string $titel, string $ulid_store)
    {
        $this->ulid = UlidService::generate();
        $this->titel = $titel;
        $this->ulid_store = $ulid_store;
    }

    public function getTitel(): string
    {
        return $this->titel;
    }

    public function getUlid(): string
    {
        return $this->ulid;
    }

    public function getUlidStorer(): string
    {
        return $this->ulid_store;
    }


}