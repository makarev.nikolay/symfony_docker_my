<?php

namespace App\Users\Infrastructure\Repository;

use App\Users\Domain\Entity\Product;
use App\Users\Domain\Entity\Store;
use App\Users\Domain\Repository\ProductRepositoryInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class ProductRepository extends ServiceEntityRepository implements ProductRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Product::class);
    }

    public function add(Product $store): void
    {
        $this->_em->persist($store);
        $this->_em->flush();
    }

    public function findByUlid(string $ulid): Product
    {
        return $this->find($ulid);
    }
}