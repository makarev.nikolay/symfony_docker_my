<?php

namespace App\Users\Infrastructure\Repository;

use App\Users\Domain\Entity\Store;
use App\Users\Domain\Repository\StoreRepositoryInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class StoreRepository extends ServiceEntityRepository implements StoreRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Store::class);
    }

    public function add(Store $store): void
    {
        $this->_em->persist($store);
        $this->_em->flush();
    }

    public function findByUlid(string $ulid): Store
    {
        return $this->find($ulid);
    }
}